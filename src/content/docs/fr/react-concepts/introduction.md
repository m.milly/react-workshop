---
title: "Introduction"
description: "Docs intro"
---

Bienvenue à l'atelier React ! Cet `atelier` vous fournira un guide étape par étape pour devenir un expert en Reactjs.

Nous couvrirons en détail les concepts suivants :

- ✅ Explication claire de la façon dont Reactjs fonctionne

- ✅ Modèles de conception React

- ✅ Astuces et astuces React

- ✅ Quelques concepts importants en programmation fonctionnelle

- ✅ Développement piloté par les tests avec React et Vitest

- ✅ React Query

- ✅ Bibliothèques de gestion d'état React (Recoil, Redux, Zustand)

- ✅ Styliser nos composants React

Et beaucoup plus de concepts que vous apprendrez au cours de cet atelier...

### Pour commencer

Pour commencer, vous pouvez obtenir tous les exemples de cet atelier en clonant ce [repository](https://gitlab.com/m.milly/react-workshop).
