---
title: "Container/Presentational Pattern"
description: "Enforce separation of concerns by separating the view from the application logic"
---

The presentational and container design pattern is a common pattern used to separate concerns between the UI components and the business logic or state management.

![Image](https://res.cloudinary.com/ddxwdqwkr/image/upload/f_auto/v1614961729/patterns.dev/prescon-pattern.jpg)

The presentational components are responsible for rendering the UI elements, such as buttons, forms, and other visual elements. They receive the necessary data and callbacks through their props and display the information accordingly. These components should be reusable, pure functions that receive their input data and render output based on that input.

On the other hand, container components are responsible for the business logic and state management of the application. They manage the data and pass it down to the presentational components as props. These components are aware of the application state and often use Redux or similar state management libraries to manage the state of the application.

The separation of concerns between presentational and container components allows for better code organization and maintainability. Presentational components can be reused in different parts of the application, while container components can manage state and other application logic in a centralized location.

### Hooks

In many cases, the Container/Presentational pattern can be replaced with React custom hooks. The usage of custom hooks made it easy for developers to add statefulness without needing a container component to provide that state.

> **Tip**: If you have useState with some useEffects, you should think about put your logic in custom hook instead of put the logic directly in the your component.
