---
title: "Introduction to styling React component"
---

There are several ways to style React components, including inline styles, CSS classes, and CSS frameworks and libraries.

Here are some popular React libraries for styling:

[Tailwind CSS](https://tailwindcss.com/): A utility-first CSS framework that provides pre-defined classes for common styles.

[MUI](https://mui.com/): A React UI framework that provides pre-built components and styles based on Google's Material Design.

[Semantic UI](https://react.semantic-ui.com/): A component library that provides pre-built UI components and styles.

We will focus on **MUI** and how I can style and customize it's components.
