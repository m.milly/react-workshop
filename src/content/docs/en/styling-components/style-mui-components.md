---
title: "Styling MUI components"
---

When it comes to styling MUI components in React, there are several ways to do it. Here are some of the most common approaches:

### Customize the component styles

1. **sx prop** :

sx prop is the most basic way to style MUI components. You can use the sx prop to add CSS styles directly to the component. For example:

```jsx
<Button sx={{ bgcolor: "primary.main", color: "white" }}>Click me</Button>
```

Sx prop has access to MUI theme so you can access to the theme like the example above `bgcolor: "primary.main"` or you can access to the whole theme object:

```jsx
<Button sx={{ bgcolor: (theme) => theme.palette.primary.main, color: "white" }}>
  Click me
</Button>
```

> Important: You should follow the theme colors and styling to make your application with same look and feel.

2. **Styled components**

To create the same css overrides in different locations across your application, create a reusable component using the `styled()` utility:

```jsx
import { styled, Button } from "@mui/material";

const StyledButton = styled(Button)({
  color: "red",
  backgroundColor: "white",
});

const App = () => {
  return <StyledButton>Click </StyledButton>;
};
```

Also, you can access to the theme through the styled utility

```jsx
import { styled, Button } from "@mui/material";

const StyledButton = styled(Button)((theme) => ({
  color: theme.palette.primary.main,
  backgroundColor: theme.palette.secondary.light,
}));

const App = () => {
  return <StyledButton>Click</StyledButton>;
};
```

3. **Global theme override**

By overriding the global theme you can managing style consistency between all components across your user interface.

Like we can do the following to override `contained` variant in all MUI Button components

```tsx
const theme: ThemeOptions = {
  components: {
    MuiButton: {
      variants: [
        {
          props: {
            variant: "contained",
          },
          style: {
            background: "white",
            color: "red",
          },
        },
      ],
    },
  },
};

function App() {
  return (
    <ThemeProvider theme={createTheme(theme)}>
      //your React components
    </ThemeProvider>
  );
}
```

Also you can access to your theme object inside your theme customization (In this way we can split out theme customization to difference files):

```ts
const theme: ThemeOptions = {
  components: {
    MuiButton: {
      variants: [
        {
          props: {
            variant: "contained",
          },
          style: ({ theme }) => ({
            backgroundColor: theme.palette.primary.main,
          }),
        },
      ],
    },
  },
};
```

### Flip between two different themes

If you build your design system and override every MUI components and your component's is modified by theme using `variant` prop

For example, lets say we have this button:

```tsx
<Button variant="contained">Click</Button>
```

we can define two different themes with different css overrides for Button components

```tsx
const oldTheme: ThemeOptions = {
  components: {
    MuiButton: {
      variants: [
        {
          props: {
            variant: "contained",
          },
          style: {
            background: "blue",
            color: "white",
          },
        },
      ],
    },
  },
};

const newTheme: ThemeOptions = {
  components: {
    MuiButton: {
      variants: [
        {
          props: {
            variant: "contained",
          },
          style: {
            background: "white",
            color: "red",
          },
        },
      ],
    },
  },
};

// In this we can change between old and new themes by change what we passed to `ThemeProvider`
```

> For more information about styling MUI components you can check out this link : https://mui.com/material-ui/customization/how-to-customize/