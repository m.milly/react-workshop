---
title: "Introduction"
description: "Docs intro"
---

Welcome to the React workshop! This `workshop` will provide to you step by step guide in every things you need to be an expert in Reactjs.

We will cover this concepts in more detail:

- ✅ **Clear explenation in how Reactjs work**

- ✅ **React design patterns**

- ✅ **React tips and tricks**

- ✅ **Some important concepts in functional programming**

- ✅ **Style our React components**

- ✅ **Useful links**

And many more concepts you learn during this workshop...

### Getting started

To get started you can get all examples inside this workshop by cloning this [repository](https://gitlab.com/m.milly/react-workshop)
