---
title: "React hooks"
description: "Explain most common React hooks"
---

In this section we will cover most of the common React hooks with some note which are important to mention.

### useState

`useState` allows us to manage state in a functional component. It takes an initial value as an argument, and returns an array with two elements: the current state value, and a function to update the state. When the state is updated, the component re-renders with the new value.

- **Lazy initializer** :

A lazy initializer is a function that returns the initial state value, and it's _only_ called during the first render of the component.

```tsx
const [state, setState] = useState(localStorage.getItem("KEY"));
```

In this example here we are calling localStorage.getItem every time the component is rerendered.

to avoid this we need to lazy initialize out state component to initial state only in the first render of the component.

```tsx
const [state, setState] = useState(() => localStorage.getItem("KEY"));
```

### useRef

`useRef` allows us to create a mutable value that persists across renders. It returns a single object with a current property that can be updated directly. It's often used to reference DOM elements, or to store a value that needs to persist across renders.

The main difference between useRef and useState is that when we update useRef the component is not rerendered.

### useReducer

`useReducer` is a React hook that provides an alternative to managing state with useState. It allows you to manage complex state and state transitions with a reducer function, similar to how it is done in Redux.

There several cases where useState is not suitable for them and use useReducer is more easy to manage you state.

This is one of the examples inside `react-lib` which use useReducer to manage complex tabs state because tabs can be pinned,ordered and saved to local storage.

```tsx
const tabsManagerReducer = (
  tabs: GenericSortableTabsMenuItemType[],
  action: ActionType
): Array<GenericSortableTabsMenuItemType> => {
  switch (action.type) {
    case TabsManagerActions.ORDER_TAP: {
      //... Update the order of tabs
    }
    case TabsManagerActions.SAVE: {
      //... Save the tabs inside local storage
    }
    case TabsManagerActions.CANCEL: {
      //... Cancel the current tabs data
    }
    default: {
      return tabs;
    }
  }
};
const [tabs, dispatch] = useReducer(
  tabsManagerReducer,
  initialTabsValue,
  initiauzerTabs
);
```

As you can see tabs state is very complex and there are many different ways to update this tabs state, So that way useReducer is more efficient in this kind of states.

### useEffect

This section very important to understand because most of React developer overuse useEffect in many unnecessary situations.

From React official documentation:

> Effects are an escape hatch from the React paradigm. They let you “step outside” of React and synchronize your components with some external system like a non-React widget, network, or the browser DOM. If there is no external system involved (for example, if you want to update a component’s state when some props or state change), you shouldn’t need an Effect. Removing unnecessary Effects will make your code easier to follow, faster to run, and less error-prone.

As documentation above, you will notice that there are speical purposes for effects like sync with **external system** and we should not rely on it to change our component data flow by updating our React states.

I will share with you some of examples from React docs to demonstrate some of these ideas.

#### Updating state based on props or state

When you have two compute new state based on props or state so you may do this thing:

```jsx
function TodoList({ todos, filter }) {
  const [newTodo, setNewTodo] = useState("");

  // 🔴 Avoid: redundant state and unnecessary Effect
  const [visibleTodos, setVisibleTodos] = useState([]);
  useEffect(() => {
    setVisibleTodos(getFilteredTodos(todos, filter));
  }, [todos, filter]);

  // ...
}
```

here not only create redundant state but your code is inefficient and simply you can do compute your derived state during rendering and not use useEffect at all:

```jsx
function Form() {
  const [firstName, setFirstName] = useState("Taylor");
  const [lastName, setLastName] = useState("Swift");
  // ✅ here we compute visibleTodos during rendering
  const visibleTodos = getFilteredTodos(todos, filter);
  // ...
}
```

The main rule is When you choose whether to put some logic into an event handler or an Effect, the main question you need to answer is what kind of logic it is from the user’s perspective. If this logic is caused by a particular interaction, keep it in the **event handler**. If it’s caused by the user seeing the component on the screen, keep it in the **Effect**.

So when your execute your effect **to update your component data flow** you are make your code is hard to maintain and inefficient.Instead, you should move your effects in your **event handlers**.

There are alot of heavy useage for useEffect inside React modules.

Here is a list of improvements we can make to move our logic outside the effect.

1. Redundant states and unnecessary effects:

```tsx
const [detergentParams, setDetergentParams] = useDetergentParams();
const config = useDetergentConfigs();

useEffect(() => {
  // 🔴 Avoid : redundant state changes and unnecessary effects
  setDetergentParams(config);
}, []);

if (!detergentParams) {
  return <EcoSpinner />;
}
```

```tsx
const config = useDetergentConfigs();

// ✅ Delete unnecessary state and effect
if (!config) {
  return <EcoSpinner />;
}
```

---

```tsx
// 🔴 Avoid :  unnecessary state
const [value, setValue] = useState<SubstanceType>("MIXTURE_OF_SUBSTANCE");

const handleChange = (newValue: SubstanceType) => {
  setValue(newValue);
  props.onChange(newValue);
};

// 🔴 Avoid :  unnecessary effects
useEffect(() => {
  setValue(props.formData || "MIXTURE_OF_SUBSTANCE");
}, [props.formData]);
```

Here value state is unnecessary and you can use props.formData directly as value and make your [component controlled](https://reactjs.org/docs/forms.html#controlled-components) with this state.

```tsx
// ✅ Delete unnecessary state and effect
const value = props.formData;

const handleChange = (newValue: SubstanceType) => {
  props.onChange(newValue);
};
```

2. Recoil state with effects

Most of effect usage with Recoil state to set it's default value because Recoil dosen't provide a way to do this but most of times it is unnecessary to use effect to set it's default value.

```tsx
const setSupplierName = useSetRecoilState(SupplierName);

useEffect(() => {
  // 🔴 Avoid : unnecessary effect to set Recoil default value
  setSupplierName(supplier && supplier.name ? supplier.name : "");
}, [supplier]);
```

Instead, you can do something like this:

```ts
// state/SupplierState.ts
let state: RecoilState<string | undefined>;

export const SupplierName = (defaultValue: string | undefined) => {
  if (!state) {
    state = atom<string | undefined>({
      key: "SupplierNameState",
      default: defaultValue,
    });
  }
  return state;
};

// containers/SupplierDescription.tsx

// ✅ Delete unnecessary effect to set Recoil state value
const [name] = useRecoilState(SupplierName("Mostafa"));
```

> Also it's worth to mention that Recoil has already built in support for running effect while updating our Recoil state. See more : [Link](https://recoiljs.org/docs/guides/atom-effects/)

3. React query with effects

Most of the effects with **React query** is to run your own effects after some cases like (query success or failed or something else) but **React query** already has built in effect which are `onError, onSettled, onSuccess`. So if you want to run your own effects please put your logic inside this callbacks.

```tsx
// 🔴 Avoid unnecessary effects after mutations or queries
useEffect(() => {
  if (supplierSaveContactsMutation.isSuccess) {
    resetDirty();
    resetInvalid();
    ...
  }
}, [supplierSaveContactsMutation]);

useEffect(() => {
  if (supplierSaveTagsMutation.isSuccess) {
    resetDirty();
    resetInvalid();
    ...
  }
}, [supplierSaveTagsMutation]);

useEffect(() => {
  if (supplierSaveRatingsMutation.isSuccess) {
    invalidateQuery();
    setIsChecking(false);
    ...
  }
}, [supplierSaveRatingsMutation]);
```

Instead move your effects into React query effects:

```tsx
// ✅ Delete unnecessary effect
const supplierSaveRatingsMutation = useMutation(API.post(...) , {onSuccess : () => {
  resetDirty();
  resetInvalid();
}});
```

In this section we cover most of the cases when the usage of useEffect is not necessary.

For more information about usage of useEffect please see this article from React official documentation : [link](https://beta.reactjs.org/learn/you-might-not-need-an-effect).

### useContext

`useContext` is a React Hook that allows components to consume values from a Context without the need to pass props through every level of the component tree. Context provides a way to share data between components without having to explicitly pass it through every level of the component tree. **IT IS NOT** a state management solution, but rather a way to **inject data into components as needed**.

Here's an example:

```jsx
import React, { useContext } from "react";

const AuthContext = React.createContext(false);

function App() {
  const [isAuthenticated, setIsAuthenticated] = React.useState(false);

  return (
    <AuthContext.Provider value={{ isAuthenticated, setIsAuthenticated }}>
      <Header />
      <Main />
      <Footer />
    </AuthContext.Provider>
  );
}
```

It worth to mention that React composition is more simpler solution ( as we will see later on in design patterns section), Here is quote from React official docs:

> If you only want to avoid passing some props through many levels, component composition is often a simpler solution than context.
