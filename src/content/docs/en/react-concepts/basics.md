---
title: "React basics"
description: "Basic information related to React framework"
---

In this section we are gonna to cover most of React basics like `Component lifecycle` and `Memorization` and many other things.

`React` is a JavaScript library that is used to build user interfaces.It was developed by Facebook and has quickly become one of the most popular libraries for front-end web development.

React works by using a virtual representation of the user interface called the [virtual DOM](https://reactjs.org/docs/faq-internals.html#what-is-the-virtual-dom) (Document Object Model). This allows React to efficiently render changes to the UI by only updating the parts of the DOM that have actually changed.

**Note:**
You can check this article to get more information about how React get benfits from virtual DOM to batch minmium updateds to the DOM : [Link](https://namansaxena-official.medium.com/react-virtual-dom-reconciliation-and-fiber-reconciler-cd33ceb0478e)

### Component Lifecycle

The React component lifecycle using hooks can be divided into three main phases:

1. **Mounting**: This phase occurs when a component is first mounted to the DOM. This phase includes the following steps:

- Run lazy initializer: Basiclly lazy initilizer is function which passed to useState/useReducer as first parameter to initialize the state with the initial value.

- Render the component: In this step React generates the virtual DOM which use to batch changes to the DOM later.

- React updates the DOM: React will batch change from VDOM to real DOM in this step.

- Run layout effects: layout effects are synchronous side effects that are executed after rendering but before the browser has painted the screen. They are used to perform tasks such as measuring DOM nodes or updating the scroll position.

- Paint the screen: in this step the browser will start painting all individual nodes according to their styles.

- Run effects: Last step is to run your effects inside useEffect to **_synchronize_** your React components with external resources such as fetching data or subscribing to a WebSocket connection (we will talk about useEffect more later on).

2. **Updating**: This phase occurs when a component's props or state change. This phase includes the following steps In addition of some steps in mount phase like render and update real DOM:

- Clean up effects: which are used for free resources when the component is unmounted from the DOM.
  The key of execution clean up effect before running the effects is to ensure that any resources
  created by the previous render are properly cleaned up before the next render is performed.

3. **Unmounting**: This phase occurs when a component is removed from the DOM and only cleanup effects are performed to make sure to free any resources when component is unmounted.

<img src="https://cdn.feather.blog/?src=https%3A%2F%2Fwww.notion.so%2Fimage%2Fhttps%3A%252F%252Fraw.githubusercontent.com%252Fdonavon%252Fhook-flow%252Fmaster%252Fhook-flow.png%3Ftable%3Dblock%26id%3D6df566c9-ca49-4d9e-b119-95845f68a078%26cache%3Dv2&optimizer=image"/>

```
Note: every React component will mounting and unmounting itself
under strict mode in React 18 is a way to help developers catch
and fix potential bugs and issues in their code, leading to more
reliable and performant applications.
```

<iframe src="https://codesandbox.io/embed/distracted-voice-goez3g?fontsize=14&hidenavigation=1&theme=dark&view=preview" 
  style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;"
  title="distracted-voice-goez3g">
</iframe>

Check out the example here : [Link](https://gitlab.com/m.milly/react-workshop/-/blob/main/src/examples/react-basics/components-life-cycle/Example.tsx)

### key prop

React assigns a “key” prop to every single element in the application.

React needs the key prop on every single JSX element basically because the virtual DOM tree need to know to which node it relates.

It worth to mention that you can programatically mount and unmount the component by changing its key and there is some situations you need to do this for resetting the component state in some cases.

Here is an example for `react-lib` to programatically mount and unmount the component by changing its key:

**react-lib\src\components\section\Section.tsx**

```tsx
export const Section = () => {
  return (
    <SectionWrapper filled={filled} id={id}>
      //...
      <SectionView views={views} state={{ sortBy, viewBy }} key={viewBy} />
      // ....
    </SectionWrapper>
  );
};
```

Here in the section view we programmatically mount and unmount **SectionView** component when the view change by changing its key prop.

### Memorization

Memorization is a technique used in React to optimize the performance of functional components. In functional components, each re-render causes the entire function body to execute again, which can be a performance bottleneck if the function is complex or expensive to execute.

Memorization involves caching the result of a function based on its input, and returning the cached result if the input hasn't changed. This can help avoid unnecessary re-execution of the function, leading to faster rendering and improved performance.

React provides several hooks that can be used for memorization, including useMemo and useCallback. useMemo can be used to memoize the result of an expensive calculation, while useCallback can be used to memoize a function reference.

Here's an example of how to use useMemo to memoize the result of an expensive calculation:

```jsx
import { useMemo } from "react";

function MyComponent({ data }) {
  const expensiveResult = useMemo(() => {
    // perform expensive calculation based on `data`
    // ...
    return result;
  }, [data]);

  return (
    <div>
      <p>Expensive result: {expensiveResult}</p>
    </div>
  );
}
```

In this example, useMemo caches the result of the expensive calculation based on the data input. If data hasn't changed since the last render, the cached result is returned, avoiding the need to re-execute the calculation.

Also you can memorize not only functions and expensive calculations but also you can memorize your React components but wrapping them with `React.memo` HOC so in this way you can rerender you React components only if there props changed.

```jsx
import React, { memo } from "react";

const MyComponent = memo((props) => {
  console.log("Rendering MyComponent");

  return (
    <div>
      <p>{props.text}</p>
    </div>
  );
});

export default MyComponent;
```

**_Important note_**: you should not use memorization in every case.
Instead, you should use them in specific cases which are:

1. Referential equality
2. Computationally expensive calculations

For more information see : [link](https://kentcdodds.com/blog/usememo-and-usecallback)
