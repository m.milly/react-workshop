---
title: "Useful links reference"
---

### Books

- [Mastering JavaScript Functional Programming](https://drive.google.com/file/d/1bBL_QWKcqA2WutRiSyw2vmjOnfadcrTQ/view?usp=sharing)

- [React design patterns](https://drive.google.com/file/d/1rakHH22-GEFeyhebPi_adQ_WwiGhu-_K/view?usp=sharing)

- [Effective Typescript](https://drive.google.com/file/d/1_IYyjJHxt4wUc4j8WgCnQ20VhumOfKpt/view?usp=sharing)

### Tutorials

- [React epics tutorials by **Kent c dodds**](https://github.com/topics/epicreact-dev)

- [Total Typescript by **Matt pocock**](https://www.totaltypescript.com/)

- [React Query Tutorials by **tkdodo**](https://tkdodo.eu/blog/practical-react-query)
