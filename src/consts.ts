export const SITE = {
  title: "React workshop",
  description: "React workshop for Ecomundo frontend developers",
  defaultLanguage: "en-us",
} as const;

export const OPEN_GRAPH = {
  image: {
    src: "https://www.datocms-assets.com/14946/1638186862-reactjs.png?auto=format&fit=max&w=1200",
    alt: "React workshop for Ecomundo frontend developers",
  },
  twitter: "ecomundo_europe",
};

export const KNOWN_LANGUAGES = {
  English: "en",
  France: "fr",
} as const;

export const KNOWN_LANGUAGE_CODES = Object.values(KNOWN_LANGUAGES);

export const GITHUB_EDIT_URL = `https://gitlab.com/m.milly/react-workshop/-/blob/main/`;

export const COMMUNITY_INVITE_URL = `https://astro.build/chat`;

// See "Algolia" section of the README for more information.
export const ALGOLIA = {
  indexName: "XXXXXXXXXX",
  appId: "XXXXXXXXXX",
  apiKey: "XXXXXXXXXX",
};

export type Sidebar = Record<
  (typeof KNOWN_LANGUAGE_CODES)[number],
  Record<string, { text: string; link: string }[]>
>;
export const SIDEBAR: Sidebar = {
  en: {
    "React concepts": [
      { text: "Introduction", link: "en/react-concepts/introduction" },
      { text: "Basics", link: "en/react-concepts/basics" },
      { text: "Hooks", link: "en/react-concepts/hooks" },
    ],

    "Design patterns": [
      { text: "Composition", link: "en/design-patterns/composition" },
      {
        text: "Container/Presentational Pattern",
        link: "en/design-patterns/container-presentational",
      },
      { text: "Render props", link: "en/design-patterns/render-props" },
      {
        text: "Compound components",
        link: "en/design-patterns/compound-components",
      },
    ],
    "FP concepts": [
      { text: "Introduction", link: "en/functional-programming/introduction" },
      {
        text: "Pipelining and composition",
        link: "en/functional-programming/pipelining-and-composition",
      },
    ],
    "Styling components": [
      { text: "Introduction", link: "en/styling-components/introduction" },
      {
        text: "Styling MUI components",
        link: "en/styling-components/style-mui-components",
      },
    ],
    "Useful links": [{ text: "Reference", link: "en/useful-links/reference" }],
  },
  fr: {
    "Concepts de React": [
      { text: "Introduction", link: "fr/react-concepts/introduction" },
      { text: "Principes de base", link: "fr/react-concepts/basics" },
      { text: "Hooks", link: "fr/react-concepts/hooks" },
    ],
  },
};
