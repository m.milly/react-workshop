import { useEffect, useLayoutEffect, useState } from "react";

function Example() {
    const [counter, setCounter] = useState(
        // Lazy initliazor 
        () => {
            console.log("lazy initliazor")
            return 0;
        })

    useEffect(() => {
        console.log("Effect")
        return () => {
            console.log("Clean up effect")
        }
    })

    useLayoutEffect(() => {
        console.log("Layout Effect")
        return () => {
            console.log("Clean up layout effect")
        }
    })

    return (
        <div>
            <h3>
                Simple counter
            </h3>
            <div style={{ display: "flex", columnGap: 1 }}>
                <button onClick={() => setCounter((prev) => prev + 1)}>Increase</button>
                <button onClick={() => setCounter((prev) => prev - 1)}>Decrease</button>
            </div>
            <div>
                {counter}
            </div>
        </div>
    );
}

export default Example;