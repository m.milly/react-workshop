import { useEffect, useState } from "react";

function App() {
  const data = { name: "John", age: 30 };

  return (
    <Parent>
      <Child data={data} />
    </Parent>
  );
}

function Parent({ children }) {
  const [counter, setCounter] = useState(0);

  useEffect(() => {
    console.log("Parent rerenderd");
  });

  return (
    <div>
      <h3>Parent component {counter}</h3>
      <button onClick={() => setCounter(counter + 1)}>button</button>
      {children}
    </div>
  );
}

function Child({ data }) {
  useEffect(() => {
    console.log("Child rerenderd");
  });

  return (
    <div>
      <h3>Child component</h3>
      <p>
        Name: {data.name}, Age: {data.age}
      </p>
    </div>
  );
}

export default App;
